window.SPA = window.SPA || {};

SPA.TreeObjModel = Backbone.Model.extend({
	defaults:{
		name: 'Default name'
	},
	initialize:function(){
		this.generateModelID();
		this.storage = this.readFromStorage();
		this.setChildrenCollection();
		this.listenTo(this,'kill',_.bind(this.removeFromStorage,this));
	},
	generateModelID:function(){
		if(!this.get('id')){
			this.set('id', this.cid+'-'+new Date().getTime());
		}
	},
	readFromStorage:function(){
		var storage = localStorage.getItem(this.get('id'));
		if(storage){
			storage = JSON.parse(storage);
			return storage;
		} else {
			return Array();
		}
	},
	createChild:function(childModel){
		this.get('children').add(childModel);
		this.saveModelToStorate(childModel);
	},
	updateModelAndStorage:function(data){
		this.set(data);
		var parentStorage = JSON.parse(localStorage.getItem(this.get('parent')));
		_.each(
			parentStorage,
			_.bind(
				function(obj){
					if (obj.id == this.get('id')){
						_.each(data, function(value,key){
							obj[key] = value;
						});
					}
				},this
			)
		);
		localStorage.setItem(this.get('parent'), JSON.stringify(parentStorage));
	},
	saveModelToStorate:function(childModel){
		var modelData = childModel.toJSON();
		modelData.children = undefined; 
		this.storage = this.readFromStorage();
		this.storage.push(modelData);
		localStorage.setItem(this.get('id'), JSON.stringify(this.storage));
	},
	removeFromStorage:function(){
		localStorage.removeItem(this.get('id')); // get rid of children if any
		var parentStorage = JSON.parse(localStorage.getItem(this.get('parent')));
		parentStorage = _.reject(parentStorage, _.bind(function(obj){ return obj.id == this.get('id') },this));
		if(parentStorage.length){
			localStorage.setItem(this.get('parent'), JSON.stringify(parentStorage));
		} else {
			localStorage.removeItem(this.get('parent'));
		}
	},
	setChildrenCollection:function(){
		var childrenCollection = new (Backbone.Collection.extend({
			model: SPA.TreeObjModel
		}))();
		this.fillChildrenCollection(childrenCollection);
		this.set('children', childrenCollection);
	},
	fillChildrenCollection:function(collection){
		if(this.storage.length){
			_.each(this.storage,function(treeItem){
				var model = new SPA.TreeObjModel(treeItem);
				collection.add(model);
			});
		}
	}
});