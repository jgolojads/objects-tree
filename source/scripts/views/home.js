window.SPA = window.SPA || {};
SPA.HomeView = Backbone.View.extend({
	events: {
	},
	initialize: function (options) {
	},
	initRootItem:function(){
		this.rootModel = new SPA.TreeObjModel({
			id:'root',
			name:'root / ..',
			root: true
		});
		var rootItem = new SPA.TreeItemSubView({treeItemModel:this.rootModel});
		this.$el.find('ul#root').append(rootItem.render().el);
	},
	render: function() {
		this.$el.html(Templates.views_home());
		this.initRootItem();
		return this;
	}
});