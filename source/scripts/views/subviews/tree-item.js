window.SPA = window.SPA || {};
SPA.TreeItemSubView = Backbone.View.extend({
	tagName:'li',
	className:'list-group-item',
	initialize: function (options) {
		this.model = options.treeItemModel;
		this.listenTo(this.model.get('children'), 'add', _.bind(this.initChildView,this));
		this.listenTo(this.model, 'kill', _.bind(this.kill,this));
	},
	setControllButtonsEventsListeners:function(){
		this.$('#addChild-'+this.model.get('id')).off('.ControllListener').on('click.ControllListener', _.bind(this.createChildInModel,this));
		this.$('#deleteSelf-'+this.model.get('id')).off('.ControllListener').on('click.ControllListener', _.bind(this.removeModel,this));
		this.$('#editable-'+this.model.get('id')).contentEditable().change( _.bind(this.updateModel,this) );
	},
	createChildInModel: function(){
		var model = new SPA.TreeObjModel({parent:this.model.get('id')});
		this.model.createChild(model);
	},
	initChildView: function (model) {
		var treeItemView = new SPA.TreeItemSubView({treeItemModel:model});
		this.$el.find('ul#root-'+this.model.get('id')).append(treeItemView.render().el);
	},
	updateModel:function(data){
		this.model.updateModelAndStorage(data.changed);
	},
	removeModel:function(){
		this.model.trigger('kill');
	},
	kill:function(){
		_.each(
			this.model.get('children').models,
			_.bind(function(child){
				child.trigger('kill')
			},this)
		);
		this.model.clear({silent:true});
		this.remove();
	},
	renderFromStorage:function(){
		_.each(this.model.get('children').models, _.bind(this.initChildView, this));
	},
	render: function() {
		var templateData = this.model.toJSON();
		this.$el.html(Templates.views_subviews_treeItem(templateData));
		this.renderFromStorage();
		this.setControllButtonsEventsListeners();
		return this;
	}
});