window.SPA = window.SPA || {};
SPA.FooterView = Backbone.View.extend({
	events: {},
	initialize: function (options) {
		this.app = options.app;
	},
	render: function() {
		this.$el.html(Templates.components_footer());
		return this;
	}
});