window.SPA = window.SPA || {};
SPA.HeaderView = Backbone.View.extend({
	events: {},
	initialize: function (options) {
		this.app = options.app;
	},
	render: function() {
		this.$el.html(Templates.components_header());
		return this;
	}
});
