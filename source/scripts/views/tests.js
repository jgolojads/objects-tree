window.SPA = window.SPA || {};
SPA.TestsView = Backbone.View.extend({
	events: {
	},
	initialize: function (options) {
	},
	runTests:function(){
		module( "Tree Item Model tests" );
		test('Model initialization', function() {
			var model = new SPA.TreeObjModel();
			model.generateModelID = sinon.spy();
			model.readFromStorage = sinon.spy();
			model.setChildrenCollection = sinon.spy();
			expect( 3 );
			model.initialize();
			ok(model.generateModelID.called, "Model 'generateModelID' called");
			ok(model.readFromStorage.called, "Model 'readFromStorage' called");
			ok(model.setChildrenCollection.called, "Model 'setChildrenCollection' called");
		});
		test("Default model attributes", function() {
			// Number of Assertions we Expect
			var model = new SPA.TreeObjModel();
			expect( 4 );
			// Default Attribute Value Assertions
			ok( model.get('id') , "model 'id' attribute must be set" );
			equal( model.get('id').split('-')[0], model.cid, "model 'id' attribute must begin with model.cid property value" );
			ok( model.get('children') , "model 'children' attribute must be set"  );
			equal( model.get('children') instanceof Backbone.Collection, true, "model 'children' attribute is instance of Backbone Collection" );
		});
		test("Model 'id' generator", function() {
			// Number of Assertions we Expect
			var model = new SPA.TreeObjModel();
			var id = model.get('id');
			model.generateModelID();
			expect( 1 );
			// Default Attribute Value Assertions
			equal( model.get('id') , id, "model 'id' generated only once per model instance" );

		});
		test("Reading from local storage", function() {
			// Number of Assertions we Expect
			var model = new SPA.TreeObjModel();
			expect( 1 );
			// Default Attribute Value Assertions
			ok( $.isArray(model.readFromStorage()), "read from storage always return Array" );
		});
		test("Save to local storage", function() {
			// Number of Assertions we Expect
			var model = new SPA.TreeObjModel();
			var childModel = new SPA.TreeObjModel();
			model.saveModelToStorate(childModel);
			childModel = childModel.toJSON();
			childModel.children = undefined; 
			var childModelString = JSON.stringify(Array(childModel));
			expect( 2 );
			// Default Attribute Value Assertions
			ok( localStorage.getItem(model.get('id')), "Children container created in localStorage" );
			equal( localStorage.getItem(model.get('id')), childModelString, "Children container hold proper model value in localStorage" );
		});
		test("Create child object", function() {
			// Number of Assertions we Expect
			var model = new SPA.TreeObjModel();

			expect( 2 );

			var childrenCount = model.get('children').length;
			strictEqual( childrenCount*1, 0, "New model has no children at initialization " );

			model.createChild(new SPA.TreeObjModel({parent:model.get('id')}));
			var newChildrenCount = model.get('children').length;
			equal( newChildrenCount*1, 1, "Child inserted in to model" );
		});
		module( "Tree Item View tests" );
		test("Render element html", function() {
			// Number of Assertions we Expect
			var model = new SPA.TreeObjModel({parent:'test'});
			var treeItemView = new SPA.TreeItemSubView({
				treeItemModel: model
			});

			expect( 1 );
			treeItemView.render();

			equal( treeItemView.$el.find('ul#root-'+model.get('id')).length, 1, "Element rendered" );

		});
		test("Add child element", function() {
			var model = new SPA.TreeObjModel({parent:'test'});
			var treeItemView = new SPA.TreeItemSubView({
				treeItemModel: model
			});
			expect( 1 );

			treeItemView.render();
			treeItemView.$el.find('#addChild-'+model.get('id')).click();
			equal(treeItemView.$el.find('ul#root-'+model.get('id')+' .tree-item-name').length, 1, "Child element rendered in the DOM");

		});
	},
	render: function() {
		this.$el.html(Templates.views_tests());
		this.runTests();
		return this;
	}
});