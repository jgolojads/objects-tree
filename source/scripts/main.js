$(function(){
	window.SPA = window.SPA || {};
	var tmpl = Templates.containers_site();
	$('body').html(tmpl);
	SPA.instance = new SPA.Router();
	//enable pushState support
	Backbone.history.start({pushState: true, root: '/'});
	//click to any link triggers check on whether link has data-bypass attribute. If attribute is there then normal page load happens, otherwise it is treated as pushState
	$(document).on('click', 'a[href]:not([data-bypass])', function(e) {
		if(!e.metaKey && !e.ctrlKey && !e.shiftKey) {
			var href = { prop: $(this).prop('href'), attr: $(this).attr('href') };
			var root = location.protocol + '//' + location.host + '/';
			if (href.prop.slice(0, root.length) === root) {
				e.preventDefault();
				Backbone.history.navigate(href.attr, {trigger:true});
			}
		}
	});
});