window.SPA = window.SPA || {};
SPA.Router = Backbone.Router.extend({

  routes: {
    'tests': 'tests',
    '': 'home',
   '*path':'home'
  },

  initialize: function(options){
    this.initialized = this.initialized || {components:[], mainView:null};
    this.components = {
      header : {name:'header', viewName: 'HeaderView', containerTagID:'header'},
      footer : {name:'footer', viewName: 'FooterView', containerTagID:'footer'}
    }
  },

  initComponent:function(options){
    var componentName = options.name,
        componentViewName = options.viewName,
        componentContainerTagID = options.containerTagID;

    this.removeComponent(componentName); // can happen that component is already initialized/active, then must be removed.

    this[componentName] = new SPA[componentViewName]({app:this});
    this[componentName].setElement($('#'+componentContainerTagID+' .container-element')).render();
    this.initialized.components.push(componentName);
  },

  removeComponent:function(componentName){
    if(this[componentName]){
      this[componentName].$el.before(Templates.containers_containerElement());
      this[componentName].remove();
    }
    this.initialized.components = _.difference(this.initialized.components, [componentName]);
  },

  loadRoute: function(View, options){
    this.removeWasteComponents(options);
    this.addRequiredComponents(options);
    this.removePreviousView();

    options.app = this; // pass to view options the app
    this.mainView = new View(options);
    this.initialized.mainView = this.mainView;
    this.mainView.setElement($('#main .container-element')).render();
  },

  removeWasteComponents: function(options){
    thisRouter = this;
    var wasteComponentsList = _.difference(this.initialized.components, options.components);
    _.each(
      wasteComponentsList,
      function(componentName){
        thisRouter.removeComponent(componentName)
      }
    );
  },

  addRequiredComponents: function(options){
    thisRouter = this;
    var initComponentsList = _.difference(options.components, this.initialized.components);
    _.each(
      initComponentsList,
      function(componentName){
        var component = thisRouter.components[componentName];
        thisRouter.initComponent(component);
      }
    );
  },

  removePreviousView:function(){
    if(this.mainView){
      this.mainView.$el.before(Templates.containers_containerElement());
      this.mainView.remove();
    }
  },

  home: function(){
    var options = {components:['header', 'footer']};
    this.loadRoute(SPA.HomeView, options);
  },

  tests: function() {
    var options = {components:['header', 'footer']};
    this.loadRoute(SPA.TestsView, options);
  }

});