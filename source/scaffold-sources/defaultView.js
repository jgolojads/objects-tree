window.SPA = window.SPA || {};
SPA.defaultViewNameView = Backbone.View.extend({
	events: {},
	initialize: function (options) {
		this.app = options.app;
	},
	render: function() {
		this.$el.html(Templates.views_defaultTemplateName());
		return this;
	}
});