var
  LIVERELOAD_PORT = 35729,
  lrSnippet = require('connect-livereload')({ port: LIVERELOAD_PORT }),
  mountFolder = function( connect, dir ) {
    return connect.static(require('path').resolve(dir));
  };

  var pushStateHook = function (url) {
    var path = require('path');
    var request = require('request'); // Need to be added into package.json
    return function (req, res, next) {
      var ext = path.extname(req.url);
      if ((ext == "" || ext === ".html") && req.url != "/") {
        req.pipe(request(url)).pipe(res);
      } else {
        next();
      }
    };
  };

module.exports = function(grunt) {

  // configure the tasks
  grunt.initConfig({

    copy: {
      build: {
        files: [
          {expand: true, flatten: true, src: ['source/index.html'], dest: 'build/', filter: 'isFile'},
          {expand: true, flatten: true, src: ['source/bower_components/bootstrap/dist/fonts/*'], dest: 'build/styles/fonts', filter: 'isFile'},
          {expand: true, cwd: 'source/', src: ['images/**'], dest: 'build/', filter: 'isFile'}
        ]
      },
    },

    clean: {
      build: {
        src: [ 'build' ]
      }
    },

    stylus: {
      build: {
        options: {
          linenos: true,
          compress: false
        },
        files: [{
          expand: true,
          cwd: 'source',
          src: [ 'styles/main.styl' ],
          dest: 'build',
          ext: '.css'
        }]
      }
    },

    cssmin: {
      build: {
        files: {
          'build/application.css': [ 'build/**/*.css' ]
        }
      }
    },

    coffee: {
      build: {
        expand: true,
        cwd: 'source',
        src: [ '**/*.coffee' ],
        dest: 'build',
        ext: '.js'
      }
    },

    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['source/scripts/**/*.js', 'source/templates/templates.js'],
        dest: 'build/scripts/scripts.js'
      },
    },

    handlebars: {
      compile: {
        files: {
          'source/templates/templates.js': [ 'source/templates/**/*.hbs' ]
        },
        options: {
          namespace: 'Templates',
          wrapped: true,
          processName: function(filePath) {
            return filePath.replace(/\.hbs$/, '').replace(/^source\/templates\//, '').replace(/\//g, '_');
          }
        }
      }
    },

    useminPrepare: {
      html: 'source/index.html',
      options: {
        dest: 'build'
      }
    },

    usemin: {
      html: ['build/{,*/}*.html'],
      css: ['build/styles/{,*/}*.css'],
      options: {
        dirs: ['build']
      }
    },

    watch: {
      stylesheets: {
        files: 'source/**/*.styl',
        tasks: [ 'stylesheets' ],
        options: {
          livereload: LIVERELOAD_PORT
        }
      },
      scripts: {
        files: 'source/scripts/**/*.js',
        tasks: [ 'concat' ],
        options: {
          livereload: LIVERELOAD_PORT
        }
      },
      handlebars: {
        files: 'source/**/*.hbs',
        tasks: [ 'handlebars', 'concat' ],
        options: {
          livereload: LIVERELOAD_PORT
        }
      }
    },

    connect: {
      options: {
        port: 4000,
        base: 'build',
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function( connect ) {
            return [
              pushStateHook("http://localhost:4000"),
              lrSnippet,
              mountFolder(connect, 'build')
            ];
          }
        }
      }
    },

    open: {
      server: {
        url: 'http://localhost:<%= connect.options.port %>'
      }
    }

  });

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-handlebars');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-open');

  // define the tasks
  grunt.registerTask(
    'stylesheets',
    'Compiles the stylesheets.',
    [ 'stylus' ] 
  );

  grunt.registerTask(
    'scripts',
    'Compiles the JavaScript files.',
    [ 'coffee' ]
  );

  grunt.registerTask(
    'run',
    'Manual usemin.',
    [ 'clean:build', 'useminPrepare', 'handlebars', 'concat', 'cssmin', 'uglify', 'copy', 'usemin', 'stylesheets', 'connect:livereload', 'open', 'watch']
  );
};