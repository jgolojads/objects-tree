below are simple instructions on how to install and build the project

**Installation and dependencies**

first install node.js
http://nodejs.org/

then install grunt and bower


```
#!javascript

$ sudo npm install grunt-cli -g
$ sudo npm install bower -g
```
for windows users: https://github.com/bower/bower#a-note-for-windows-users

then get the repository


```
#!javascript

$ git clone https://jgolojads@bitbucket.org/jgolojads/objects-tree.git
```


then go to the project directory


```
#!javascript

$ cd /objects-tree
```


and then install dependencies


```
#!javascript

$ npm install && bower install
```


**Building & Run Application**


```
#!javascript

$ grunt run
```
This will build application to the project's '/build' directory,then start it on http://localhost:4000 and open default browser window with application. The watcher will be watching changes on source files and restarting application on every change.

Happy cloning!